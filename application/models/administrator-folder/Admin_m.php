<?php
/**
*
*/
class Admin_m extends CI_Model{

  function __construct(){
    parent::__construct();
  }

  function storeSiswa(){
    $nis = $_POST['nis'];
    $nama = $_POST['nama'];
    $kelas = $_POST['kelas'];
    $tahun = $_POST['tahun'];
    $data = array(
      'nis' => $nis,
      'nama_siswa' => $nama,
      'kelas' => $kelas,
      'tahun' => $tahun
    );
    $query = $this->db->insert('spk_datasiswa', $data);
  }
    function showSiswa(){
    $query  = $this->db->query(
              "SELECT *
               FROM spk_datasiswa
               ORDER BY nis DESC");
    return $query->result_array();
    }
    function showSiswaByClass($kelas){
      $query= $this->db->query(
              "SELECT nis, nama_siswa
               FROM spk_datasiswa
               WHERE nis NOT IN (SELECT nis FROM spk_datarating)
               AND kelas = '$kelas'
               GROUP BY nis");
       if($query->num_rows() > 0){
             return $query->result();
         } else {
             return FALSE;
         }
    }
  //------------------------------------------------------------------------//
  //------------------------------------------------------------------------//

  function storeUser(){
    $nama = $_POST['nama'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $data = array(
          'nama_user' => $nama,
          'username' => $username,
          'password' => $password,
          'level' => "SPK-ADMIN");
    $query = $this->db->insert('spk_datauser', $data);
  }
  function showUser(){
  $query  = $this->db->query(
            "SELECT *
             FROM spk_datauser
             ORDER BY id_user DESC");
  return $query->result_array();
  }

  //------------------------------------------------------------------------//
  //------------------------------------------------------------------------//

  function storeKriteria(){
    $kriteria = $_POST['kriteria'];
    $bobot_ipa = $_POST['bobot_ipa'];
    $bobot_ips = $_POST['bobot_ips'];
    $data = array(
          'nama_kriteria' => $kriteria,
          'bobot_IPA' => $bobot_ipa,
          'bobot_IPS' => $bobot_ips);
    $query = $this->db->insert('spk_datakriteria', $data);
  }

  function showKriteria(){
  $query  = $this->db->query(
            "SELECT *
             FROM spk_datakriteria
             ORDER BY id_kriteria DESC");
  return $query->result_array();
  }

  //
  function storeNilai(){
		$nama = $this->input->post('nama');

		$mat = $this->input->post('mat');
		$fis = $this->input->post('fis');
		$kim = $this->input->post('kim');
		$bio = $this->input->post('bio');
		$eko = $this->input->post('eko');
		$sos = $this->input->post('sos');
		$geo = $this->input->post('geo');

		$minat_ipa = $this->input->post('minat_ipa');
    $minat_ips = $this->input->post('minat_ips');
		$psiko = $this->input->post('psiko');

		if ($mat> 90) {$rat_mat = 5;}elseif ($mat> 80) {$rat_mat = 4;}elseif ($mat> 70) {$rat_mat = 3;}elseif ($mat> 60) {$rat_mat = 2;}else{$rat_mat = 1;}
    if ($fis> 90) {$rat_fis = 5;}elseif ($fis> 80) {$rat_fis = 4;}elseif ($fis> 70) {$rat_fis = 3;}elseif ($fis> 60) {$rat_fis = 2;}else{$rat_fis = 1;}
    if ($kim> 90) {$rat_kim = 5;}elseif ($kim> 80) {$rat_kim = 4;}elseif ($kim> 70) {$rat_kim = 3;}elseif ($kim> 60) {$rat_kim = 2;}else{$rat_kim = 1;}
    if ($bio> 90) {$rat_bio = 5;}elseif ($bio> 80) {$rat_bio = 4;}elseif ($bio> 70) {$rat_bio = 3;}elseif ($bio> 60) {$rat_bio = 2;}else{$rat_bio = 1;}
    if ($eko> 90) {$rat_eko = 5;}elseif ($eko> 80) {$rat_eko = 4;}elseif ($eko> 70) {$rat_eko = 3;}elseif ($eko> 60) {$rat_eko = 2;}else{$rat_eko = 1;}
    if ($sos> 90) {$rat_sos = 5;}elseif ($sos> 80) {$rat_sos = 4;}elseif ($sos> 70) {$rat_sos = 3;}elseif ($sos> 60) {$rat_sos = 2;}else{$rat_sos = 1;}
    if ($geo> 90) {$rat_geo = 5;}elseif ($geo> 80) {$rat_geo = 4;}elseif ($geo> 70) {$rat_geo = 3;}elseif ($geo> 60) {$rat_geo = 2;}else{$rat_geo = 1;}

    if ($psiko> 120) {
      $rat_psiko = 5;
    }elseif ($psiko> 109) {
      $rat_psiko = 4;
    }elseif ($psiko> 89) {
      $rat_psiko = 3;
    }elseif ($psiko> 79) {
      $rat_psiko = 2;
    }else{
      $rat_psiko = 1;
    }
    $datarating =  array(
							'nis' => $nama,
							'nilai_mat'	=> $rat_mat,
							'nilai_fisika'	=> $rat_fis,
							'nilai_kimia'	=> $rat_kim,
							'nilai_biologi'	=> $rat_bio,
							'nilai_ekonomi'	=> $rat_eko,
							'nilai_sosiologi'	=> $rat_sos,
							'nilai_geografi'	=> $rat_geo,
              'nilai_minat_ipa' => $minat_ipa,
              'nilai_minat_ips' => $minat_ips,
              'nilai_psikotest' => $rat_psiko
							);
    $datanilai =  array(
							'nis' => $nama,
							'nilai_mat'	=> $mat,
							'nilai_fisika'	=> $fis,
							'nilai_kimia'	=> $kim,
							'nilai_biologi'	=> $bio,
							'nilai_ekonomi'	=> $eko,
							'nilai_sosiologi'	=> $sos,
							'nilai_geografi'	=> $geo,
              'minat_ipa' => $minat_ipa,
              'minat_ips' => $minat_ips,
              'nilai_psikotest' => $psiko
							);
		$this->db->insert('spk_datarating', $datarating);
    $this->db->insert('spk_datanilai', $datanilai);
  }
  function showTabelRating(){
    $query = $this->db->query(
             "SELECT spk_datasiswa.kelas, spk_datasiswa.nama_siswa,

                     spk_datarating.nis, spk_datarating.nilai_mat, spk_datarating.nilai_fisika, spk_datarating.nilai_kimia, spk_datarating.nilai_biologi,
                     spk_datarating.nilai_ekonomi, spk_datarating.nilai_sosiologi, spk_datarating.nilai_geografi, spk_datarating.nilai_psikotest,
                     spk_datarating.nilai_minat_ipa, spk_datarating.nilai_minat_ips,

                     spk_datanilai.nis, spk_datanilai.nilai_mat as mat, spk_datanilai.nilai_fisika as fis, spk_datanilai.nilai_kimia as kim, spk_datanilai.nilai_biologi as bio,
                     spk_datanilai.nilai_ekonomi as eko, spk_datanilai.nilai_sosiologi as sos, spk_datanilai.nilai_geografi as geo, spk_datanilai.nilai_psikotest as psiko,
                     spk_datanilai.minat_ipa as mipa, spk_datanilai.minat_ips as mips
              FROM spk_datarating
              LEFT JOIN spk_datasiswa ON spk_datasiswa.nis = spk_datarating.nis
              LEFT JOIN spk_datanilai ON spk_datanilai.nis = spk_datasiswa.nis
              ORDER BY spk_datarating.id_rating DESC");
    return $query->result_array();
  }
  function showMaxRating(){
    $query = $this->db->query(
              "SELECT nis, MAX(nilai_mat) as max_mat, MAX(nilai_fisika) as max_fis, MAX(nilai_kimia) as max_kim,
                      MAX(nilai_biologi) as max_bio, MAX(nilai_ekonomi) as max_eko, MAX(nilai_sosiologi) as max_sos,
                      MAX(nilai_geografi) as max_geo, MAX(nilai_minat_ipa) as max_ipa, MAX(nilai_minat_ips) as max_ips,
                      MAX(nilai_psikotest) as max_psiko
               FROM spk_datarating");
    return $query->result_array();
  }

  function quick_count(){
    $query1 = $this->db->query(
              "SELECT * FROM spk_datarating
              ORDER BY id_rating DESC");

    $query2 = $this->db->query(
              "SELECT MAX(nilai_mat) as max_mat, MAX(nilai_fisika) as max_fis, MAX(nilai_kimia) as max_kim,
                      MAX(nilai_biologi) as max_bio, MAX(nilai_ekonomi) as max_eko, MAX(nilai_sosiologi) as max_sos,
                      MAX(nilai_geografi) as max_geo, MAX(nilai_minat_ipa) as max_ipa, MAX(nilai_minat_ips) as max_ips,
                      MAX(nilai_psikotest) as max_psiko
               FROM spk_datarating");
    $query3 = $this->db->query(
              "SELECT
                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Matematika') AS MAT_IPA,

                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Fisika') AS FIS_IPA,

                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Biologi') AS BIO_IPA,

                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Kimia') AS KIM_IPA,

                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Ekonomi') AS EKO_IPA,

                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Sosiologi') AS SOS_IPA,

                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Geografi') AS GEO_IPA,

                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Minat di Jurusan IPA') AS MINAT_IPA_IPA,

                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Minat di Jurusan IPS') AS MINAT_IPS_IPA,

                 (SELECT bobot_IPA
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Psikotest') AS PSIKO_IPA,


                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Matematika') AS MAT_IPS,

                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Fisika') AS FIS_IPS,

                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Biologi') AS BIO_IPS,

                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Kimia') AS KIM_IPS,

                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Ekonomi') AS EKO_IPS,

                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Sosiologi') AS SOS_IPS,

                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Geografi') AS GEO_IPS,

                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Minat di Jurusan IPA') AS MINAT_IPA_IPS,

                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Minat di Jurusan IPS') AS MINAT_IPS_IPS,

                 (SELECT bobot_IPS
                 FROM spk_datakriteria
                 WHERE nama_kriteria ='Psikotest') AS PSIKO_IPS


               FROM spk_datakriteria
               LIMIT 0,1");

   foreach ($query3->result() as $data_kriteria) {
     $krit_mat_ipa = $data_kriteria->MAT_IPA;
     $krit_fis_ipa = $data_kriteria->FIS_IPA;
     $krit_kim_ipa = $data_kriteria->BIO_IPA;
     $krit_bio_ipa = $data_kriteria->KIM_IPA;
     $krit_eko_ipa = $data_kriteria->EKO_IPA;
     $krit_sos_ipa = $data_kriteria->SOS_IPA;
     $krit_geo_ipa = $data_kriteria->GEO_IPA;
     $krit_minat_ipa_ipa = $data_kriteria->MINAT_IPA_IPA;
     $krit_minat_ips_ipa = $data_kriteria->MINAT_IPS_IPA;
     $krit_psiko_ipa = $data_kriteria->PSIKO_IPA;

     $krit_mat_ips = $data_kriteria->MAT_IPS;
     $krit_fis_ips = $data_kriteria->FIS_IPS;
     $krit_kim_ips = $data_kriteria->BIO_IPS;
     $krit_bio_ips = $data_kriteria->KIM_IPS;
     $krit_eko_ips = $data_kriteria->EKO_IPS;
     $krit_sos_ips = $data_kriteria->SOS_IPS;
     $krit_geo_ips = $data_kriteria->GEO_IPS;
     $krit_minat_ipa_ips = $data_kriteria->MINAT_IPA_IPS;
     $krit_minat_ips_ips = $data_kriteria->MINAT_IPS_IPS;
     $krit_psiko_ips = $data_kriteria->PSIKO_IPS;
   }

   foreach ($query2->result() as $data_max_rating) {
     $max_mat = $data_max_rating->max_mat;
     $max_fis = $data_max_rating->max_fis;
     $max_kim = $data_max_rating->max_kim;
     $max_bio = $data_max_rating->max_bio;
     $max_eko = $data_max_rating->max_eko;
     $max_sos = $data_max_rating->max_sos;
     $max_geo = $data_max_rating->max_geo;
     $max_minat_ipa = $data_max_rating->max_ipa;
     $max_minat_ips = $data_max_rating->max_ips;
     $max_psiko = $data_max_rating->max_psiko;
   }
//------------------------------------------------------------------------------
    foreach ($query1->result() as $data_rating) {
      $nis = $data_rating->nis;
      $mat = $data_rating->nilai_mat;
      $fis = $data_rating->nilai_fisika;
      $kim = $data_rating->nilai_kimia;
      $bio = $data_rating->nilai_biologi;
      $eko = $data_rating->nilai_ekonomi;
      $sos = $data_rating->nilai_sosiologi;
      $geo = $data_rating->nilai_geografi;
      $minat_ipa = $data_rating->nilai_minat_ipa;
      $minat_ips = $data_rating->nilai_minat_ips;
      $psiko = $data_rating->nilai_psikotest;

      $r_mat_ipa = number_format(($mat / $max_mat) * $krit_mat_ipa,2);
      $r_fis_ipa = number_format(($fis / $max_fis) * $krit_fis_ipa,2);
      $r_kim_ipa = number_format(($kim / $max_kim) * $krit_kim_ipa,2);
      $r_bio_ipa = number_format(($bio / $max_bio) * $krit_bio_ipa,2);
      $r_eko_ipa = number_format(($eko / $max_eko) * $krit_eko_ipa,2);
      $r_sos_ipa = number_format(($sos / $max_sos) * $krit_sos_ipa,2);
      $r_geo_ipa = number_format(($geo / $max_geo) * $krit_geo_ipa,2);
      $r_minat_ipa_ipa = number_format(($minat_ipa / $max_minat_ipa) * $krit_minat_ipa_ipa,2);
      $r_minat_ipa_ipa = number_format(($minat_ips / $max_minat_ips) * $krit_minat_ips_ipa,2);
      $r_psiko_ipa = number_format(($psiko / $max_psiko) * $krit_psiko_ipa,2);
      $data_insert_ipa = array(
                    'nis' => $nis,
                    'nilai_mat' => $r_mat_ipa,
                    'nilai_fisika' => $r_fis_ipa,
                    'nilai_kimia' => $r_kim_ipa,
                    'nilai_biologi' => $r_bio_ipa,
                    'nilai_ekonomi' => $r_eko_ipa,
                    'nilai_sosiologi' => $r_sos_ipa,
                    'nilai_geografi' => $r_geo_ipa,
                    'nilai_minat_ipa' => $r_minat_ipa_ipa,
                    'nilai_minat_ips' => $r_minat_ipa_ipa,
                    'nilai_psikotest' => $r_psiko_ipa);
      $this->db->insert('spk_finalipa', $data_insert_ipa);

      $r_mat_ips = number_format(($mat / $max_mat) * $krit_mat_ips,2);
      $r_fis_ips = number_format(($fis / $max_fis) * $krit_fis_ips,2);
      $r_kim_ips = number_format(($kim / $max_kim) * $krit_kim_ips,2);
      $r_bio_ips = number_format(($bio / $max_bio) * $krit_bio_ips,2);
      $r_eko_ips = number_format(($eko / $max_eko) * $krit_eko_ips,2);
      $r_sos_ips = number_format(($sos / $max_sos) * $krit_sos_ips,2);
      $r_geo_ips = number_format(($geo / $max_geo) * $krit_geo_ips,2);
      $r_minat_ipa_ips = number_format(($minat_ipa / $max_minat_ipa) * $krit_minat_ipa_ips,2);
      $r_minat_ipa_ips = number_format(($minat_ips / $max_minat_ips) * $krit_minat_ips_ips,2);
      $r_psiko_ips = number_format(($psiko / $max_psiko) * $krit_psiko_ips,2);
      $data_insert_ips = array(
                    'nis' => $nis,
                    'nilai_mat' => $r_mat_ips,
                    'nilai_fisika' => $r_fis_ips,
                    'nilai_kimia' => $r_kim_ips,
                    'nilai_biologi' => $r_bio_ips,
                    'nilai_ekonomi' => $r_eko_ips,
                    'nilai_sosiologi' => $r_sos_ips,
                    'nilai_geografi' => $r_geo_ips,
                    'nilai_minat_ipa' => $r_minat_ipa_ips,
                    'nilai_minat_ips' => $r_minat_ipa_ips,
                    'nilai_psikotest' => $r_psiko_ips);
      $this->db->insert('spk_finalips', $data_insert_ips);
    }
  }

  //

}
  ?>
