<?php
/**
 *
 */
class Kriteria_m extends CI_Model{

  function __construct(){
    parent::__construct();
  }
  function showKriteriaMATforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Matematika'");
    return $query->result_array();
  }
  function showKriteriaFISforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Fisika'");
    return $query->result_array();
  }
  function showKriteriaKIMforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Kimia'");
    return $query->result_array();
  }
  function showKriteriaBIOforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Biologi'");
    return $query->result_array();
  }
  function showKriteriaEKOforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Ekonomi'");
    return $query->result_array();
  }
  function showKriteriaSOSforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Sosiologi'");
    return $query->result_array();
  }
  function showKriteriaGEOforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Geografi'");
    return $query->result_array();
  }
  function showKriteriaMINAT_IPAforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Minat di Jurusan IPA'");
    return $query->result_array();
  }
  function showKriteriaMINAT_IPSforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Minat di Jurusan IPS'");
    return $query->result_array();
  }
  function showKriteriaPSIKOforIPA(){
    $query = $this->db->query(
              "SELECT bobot_IPA
               FROM spk_datakriteria
               WHERE nama_kriteria ='Psikotest'");
    return $query->result_array();
  }

//=============================================================================//
//=============================================================================//

function showKriteriaMATforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Matematika'");
  return $query->result_array();
}
function showKriteriaFISforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Fisika'");
  return $query->result_array();
}
function showKriteriaKIMforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Kimia'");
  return $query->result_array();
}
function showKriteriaBIOforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Biologi'");
  return $query->result_array();
}
function showKriteriaEKOforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Ekonomi'");
  return $query->result_array();
}
function showKriteriaSOSforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Sosiologi'");
  return $query->result_array();
}
function showKriteriaGEOforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Geografi'");
  return $query->result_array();
}
function showKriteriaMINAT_IPAforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Minat di Jurusan IPA'");
  return $query->result_array();
}
function showKriteriaMINAT_IPSforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Minat di Jurusan IPS'");
  return $query->result_array();
}
function showKriteriaPSIKOforIPS(){
  $query = $this->db->query(
            "SELECT bobot_IPS
             FROM spk_datakriteria
             WHERE nama_kriteria ='Psikotest'");
  return $query->result_array();
}





}
 ?>
