<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['adminLogin'] = 'administrator/aksiLogin';
$route['adminLogout'] = 'administrator/aksiLogout';
$route['data-siswa'] = 'administrator/halamanSiswa';
$route['data-user'] = 'administrator/halamanUser';
$route['data-kriteria'] = 'administrator/halamanKriteria';
$route['data-bobot'] = 'administrator/halamanBobot';

//
$route['storeSiswa'] = 'administrator/storeSiswa';
$route['storeUser'] = 'administrator/storeUser';
$route['storeKriteria'] = 'administrator/storeKriteria';
//

//
$route['firstStep'] = 'administrator/firstStep';
$route['storeNilai'] = 'administrator/storeNilai';
$route['quick_count'] = 'administrator/quick_count';
//

$route['default_controller'] = 'administrator';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
