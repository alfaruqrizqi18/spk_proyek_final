<?php
/**
 *
 */
class Api extends CI_Controller{

  function __construct(){
    parent::__construct();
  }

  function api_datasiswa(){
    $query  = $this->db->query(
              "SELECT *
               FROM spk_datasiswa
               ORDER BY nis ASC")->result_array();
    header('Content-Type: application/json');
    print_r(json_encode($query));
  }
}
 ?>
