<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('login-folder/login_m');
		$this->load->model('administrator-folder/admin_m');
		$this->load->model('administrator-folder/kriteria_m');
	}
	function index(){
		$db['title_master'] = "Dashboard | Sistem Pendukung Keputusan | Penjurusan SMAN 7 Kediri";
		if ($this->session->userdata('LEVEL_USER_SPK') == "SPK-ADMIN") {
			$this->load->view('template-master-admin/header',$db);
			$this->load->view('administrator/index');
		}else if($this->session->userdata('LEVEL_USER_SPK') == ""){
			$this->halaman_login();
		}
	}
	function halaman_login(){
		$db['title_master'] = "Halaman Login | Sistem Pendukung Keputusan | Penjurusan SMAN 7 Kediri";
		$this->load->view('login-folder/login',$db);
	}
	function aksiLogin(){
		$this->login_m->aksiLogin();
	}
	function aksiLogout(){
		$this->login_m->aksiLogout();
	}

	//=========================================
	function halamanSiswa(){
		if ($this->session->userdata('LEVEL_USER_SPK') == "SPK-ADMIN") {
			$db['title_master'] = "Data Master Siswa | Sistem Pendukung Keputusan | Penjurusan SMAN 7 Kediri";
			$this->load->view('template-master-admin/header',$db);
			$this->load->view('administrator/data-siswa');
		}else{
			redirect(base_url());
		}
	}

	//--------------------------------------------------------//
	//--------------------------------------------------------//
	function storeSiswa(){
		$this->admin_m->storeSiswa();
	}
	function tabelSiswa(){
		$this->load->view('administrator/tabel-siswa-json');
	}
	//--------------------------------------------------------//
	//--------------------------------------------------------//


	//--------------------------------------------------------//
	//--------------------------------------------------------//
	function halamanUser(){
		if ($this->session->userdata('LEVEL_USER_SPK') == "SPK-ADMIN") {
			$db['title_master'] = "Data Master User | Sistem Pendukung Keputusan | Penjurusan SMAN 7 Kediri";
			$this->load->view('template-master-admin/header',$db);
			$this->load->view('administrator/data-user');
		}else{
			redirect(base_url());
		}
	}
	function storeUser(){
		$this->admin_m->storeUser();
	}
	function tabelUser(){
		$db['showUser'] = $this->admin_m->showUser();
		$this->load->view('administrator/tabel-user',$db);
	}
	//--------------------------------------------------------//
	//--------------------------------------------------------//


	//--------------------------------------------------------//
	//--------------------------------------------------------//
	function halamanKriteria(){
		if ($this->session->userdata('LEVEL_USER_SPK') == "SPK-ADMIN") {
			$db['title_master'] = "Data Master Kriteria | Sistem Pendukung Keputusan | Penjurusan SMAN 7 Kediri";
			$this->load->view('template-master-admin/header',$db);
			$this->load->view('administrator/data-kriteria');
		}else{
			redirect(base_url());
		}
	}
	function storeKriteria(){
		$this->admin_m->storeKriteria();
	}
	function tabelKriteria(){
		$db['showKriteria'] = $this->admin_m->showKriteria();
		$this->load->view('administrator/tabel-kriteria',$db);
	}
	//--------------------------------------------------------//
	//--------------------------------------------------------//
	function firstStep(){
		$db['title_master'] = "Mulai Penjurusan | Sistem Pendukung Keputusan | Penjurusan SMAN 7 Kediri";
		$this->load->view('template-master-admin/header',$db);
		$this->load->view('administrator/first-step');
	}
	function showSiswaByClass(){
		$kelas = $this->input->post('kelas');  //mengambil post data yang dijabarkan pada javascript yaitu type: "POST"
		$query = $this->admin_m->showSiswaByClass($kelas);  //mengambil data dari database melalui model mcombox
		foreach ($query as $data) {
			echo "<option value=$data->nis>$data->nama_siswa</option>";
		}
	}
	function storeNilai(){
		$this->admin_m->storeNilai();
	}
	function showTabelRating(){
		$db['showTabelRating'] = $this->admin_m->showTabelRating();
		$db['showMaxRating'] = $this->admin_m->showMaxRating();
		$this->load->view('administrator/tabel-rating',$db);
	}
	function showTabelRatingIPA(){
		$db['showTabelRating'] = $this->admin_m->showTabelRating();
		$db['showMaxRating'] = $this->admin_m->showMaxRating();

		$db['showKriteriaMATforIPA'] = $this->kriteria_m->showKriteriaMATforIPA();
		$db['showKriteriaFISforIPA'] = $this->kriteria_m->showKriteriaFISforIPA();
		$db['showKriteriaKIMforIPA'] = $this->kriteria_m->showKriteriaKIMforIPA();
		$db['showKriteriaBIOforIPA'] = $this->kriteria_m->showKriteriaBIOforIPA();
		$db['showKriteriaEKOforIPA'] = $this->kriteria_m->showKriteriaEKOforIPA();
		$db['showKriteriaSOSforIPA'] = $this->kriteria_m->showKriteriaSOSforIPA();
		$db['showKriteriaGEOforIPA'] = $this->kriteria_m->showKriteriaGEOforIPA();
		$db['showKriteriaMINAT_IPAforIPA'] = $this->kriteria_m->showKriteriaMINAT_IPAforIPA();
		$db['showKriteriaMINAT_IPSforIPA'] = $this->kriteria_m->showKriteriaMINAT_IPSforIPA();
		$db['showKriteriaPSIKOforIPA'] = $this->kriteria_m->showKriteriaPSIKOforIPA();
		$this->load->view('administrator/tabel-rating-ipa',$db);
	}
	function showTabelRatingIPS(){
		$db['showTabelRating'] = $this->admin_m->showTabelRating();
		$db['showMaxRating'] = $this->admin_m->showMaxRating();

		$db['showKriteriaMATforIPS'] = $this->kriteria_m->showKriteriaMATforIPS();
		$db['showKriteriaFISforIPS'] = $this->kriteria_m->showKriteriaFISforIPS();
		$db['showKriteriaKIMforIPS'] = $this->kriteria_m->showKriteriaKIMforIPS();
		$db['showKriteriaBIOforIPS'] = $this->kriteria_m->showKriteriaBIOforIPS();
		$db['showKriteriaEKOforIPS'] = $this->kriteria_m->showKriteriaEKOforIPS();
		$db['showKriteriaSOSforIPS'] = $this->kriteria_m->showKriteriaSOSforIPS();
		$db['showKriteriaGEOforIPS'] = $this->kriteria_m->showKriteriaGEOforIPS();
		$db['showKriteriaMINAT_IPAforIPS'] = $this->kriteria_m->showKriteriaMINAT_IPAforIPS();
		$db['showKriteriaMINAT_IPSforIPS'] = $this->kriteria_m->showKriteriaMINAT_IPSforIPS();
		$db['showKriteriaPSIKOforIPS'] = $this->kriteria_m->showKriteriaPSIKOforIPS();
		$this->load->view('administrator/tabel-rating-ips',$db);
	}
	function showTabelRatingFINAL(){
		$db['showTabelRating'] = $this->admin_m->showTabelRating();
		$db['showMaxRating'] = $this->admin_m->showMaxRating();

		$db['showKriteriaMATforIPA'] = $this->kriteria_m->showKriteriaMATforIPA();
		$db['showKriteriaFISforIPA'] = $this->kriteria_m->showKriteriaFISforIPA();
		$db['showKriteriaKIMforIPA'] = $this->kriteria_m->showKriteriaKIMforIPA();
		$db['showKriteriaBIOforIPA'] = $this->kriteria_m->showKriteriaBIOforIPA();
		$db['showKriteriaEKOforIPA'] = $this->kriteria_m->showKriteriaEKOforIPA();
		$db['showKriteriaSOSforIPA'] = $this->kriteria_m->showKriteriaSOSforIPA();
		$db['showKriteriaGEOforIPA'] = $this->kriteria_m->showKriteriaGEOforIPA();
		$db['showKriteriaMINAT_IPAforIPA'] = $this->kriteria_m->showKriteriaMINAT_IPAforIPA();
		$db['showKriteriaMINAT_IPSforIPA'] = $this->kriteria_m->showKriteriaMINAT_IPSforIPA();
		$db['showKriteriaPSIKOforIPA'] = $this->kriteria_m->showKriteriaPSIKOforIPA();
		//------------------------------------------------------------------------------------//
		$db['showKriteriaMATforIPS'] = $this->kriteria_m->showKriteriaMATforIPS();
		$db['showKriteriaFISforIPS'] = $this->kriteria_m->showKriteriaFISforIPS();
		$db['showKriteriaKIMforIPS'] = $this->kriteria_m->showKriteriaKIMforIPS();
		$db['showKriteriaBIOforIPS'] = $this->kriteria_m->showKriteriaBIOforIPS();
		$db['showKriteriaEKOforIPS'] = $this->kriteria_m->showKriteriaEKOforIPS();
		$db['showKriteriaSOSforIPS'] = $this->kriteria_m->showKriteriaSOSforIPS();
		$db['showKriteriaGEOforIPS'] = $this->kriteria_m->showKriteriaGEOforIPS();
		$db['showKriteriaMINAT_IPAforIPS'] = $this->kriteria_m->showKriteriaMINAT_IPAforIPS();
		$db['showKriteriaMINAT_IPSforIPS'] = $this->kriteria_m->showKriteriaMINAT_IPSforIPS();
		$db['showKriteriaPSIKOforIPS'] = $this->kriteria_m->showKriteriaPSIKOforIPS();
		$this->load->view('administrator/tabel-rating-final',$db);
	}
	function quick_count(){
		$this->admin_m->quick_count();
	}

}
