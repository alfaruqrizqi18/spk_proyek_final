<!DOCTYPE html>
<html lang="en">
<base href="<?php echo base_url(); ?>" target="">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="<?php echo $title_master ?>">
  <meta name="author" content="">
  <link rel="shortcut icon" href="assets/img/logo-fav.png">
  <title><?php echo $title_master; ?></title>
  <link rel="stylesheet" type="text/css" href="assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
  <link rel="stylesheet" type="text/css" href="assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="assets/css/style.min.css" type="text/css"/>
  </head>
  <body class="be-splash-screen">
    <div class="be-wrapper be-login">
      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="splash-container">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
              <div class="panel-heading"><img src="assets/img/logo-xx.png" alt="logo" width="102" height="27" class="logo-img"><span class="splash-description">Masukkan informasi Username & Password disini.</span></div>
              <?php if ($this->session->flashdata('message') != null || $this->session->flashdata('message') != ""): ?>
              <div role="alert" class="alert alert-danger alert-dismissible">
                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><span class="icon mdi mdi-close-circle-o"></span><strong><?php echo $this->session->flashdata('message'); ?></strong>
              </div>
              <?php endif; ?>
              <div class="panel-body">
                <form action="adminLogin" method="post">
                  <div class="form-group">
                    <input id="username" type="text" placeholder="Username" autocomplete="off" class="form-control" required="" name="username" autofocus="">
                  </div>
                  <div class="form-group">
                    <input id="password" type="password" placeholder="Password" class="form-control" required="" name="password">
                  </div>
                  <div class="form-group login-submit">
                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Masuk</button>
                  </div>
                </form>
              </div>
            </div>
            <div class="splash-footer">
              <span><a href="#">Apa Sistem Pendukung Keputusan ?</a> | <a href="#">Tentang Aplikasi</a> | <a href="#">Team</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/main.js" type="text/javascript"></script>
    <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();
    });

    </script>
  </body>
  </html>
