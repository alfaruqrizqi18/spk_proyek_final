<table id="table1" class="table table-hover table-fw-widget">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nama Kriteria</th>
      <th>IPA</th>
      <th>IPS</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($showKriteria as $data): ?>
      <tr class="odd gradeX">
        <td><?php echo $data['id_kriteria']; ?></td>
        <td><?php echo $data['nama_kriteria']; ?></td>
        <td><?php echo $data['bobot_IPA']; ?></td>
        <td><?php echo $data['bobot_IPS']; ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<script src="assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
<script src="assets/js/app-tables-datatables.js" type="text/javascript"></script>


<script type="text/javascript">
$(document).ready(function(){
  //initialize the javascript
  App.init();
  App.dataTables();
});
</script>
