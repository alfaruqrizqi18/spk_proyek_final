<div class="be-content">
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">Data Master Siswa - Page rendered <strong>({elapsed_time})</strong></div>
          <div class="panel-body">
            <form id="form-input">
              <div class="form-group col-md-2">
                <input type="number" required placeholder="NIS Siswa..." autocomplete="off" autofocus class="form-control" name="nis" id="nis">
              </div>
              <div class="form-group col-md-3">
                <input type="text" required placeholder="Nama Siswa..." autocomplete="off" class="form-control" name="nama" id="nama">
              </div>
              <div class="form-group col-md-2">
                <select class="form-control" name="kelas" id="kelas">
                  <option value="">Kelas : </option>
                  <option value="A">Kelas : A</option>
                  <option value="B">Kelas : B</option>
                  <option value="C">Kelas : C</option>
                  <option value="D">Kelas : D</option>
                  <option value="E">Kelas : E</option>
                  <option value="F">Kelas : F</option>
                  <option value="G">Kelas : G</option>
                  <option value="H">Kelas : H</option>
                  <option value="I">Kelas : I</option>
                </select>
              </div>
              <div class="form-group col-md-2">
                <input type="number" required placeholder="Tahun ajaran..." autocomplete="off" class="form-control" name="tahun" id="tahun" >
              </div>
              <div class="col-md-3">
                <button type="submit" class="btn btn-space btn-success btn-xl">
                  <i class="icon icon-left mdi mdi-cloud-done"></i>
                  Tambahkan Siswa</button>
                  <button type="button" class="btn btn-space btn-primary btn-xl">
                    <i class="icon icon-left mdi mdi-brush" ></i>Reset</button>
                  </form>
                </div>
              </div>
              <div class="panel panel-default panel-table">
                <div class="panel-body" >
                  <div class="col-sm-12">
                    <div class="panel panel-default panel-table">
                      <div class="panel-body" id="tabel-siswa">
                        <table id="table1" class="table table-hover table-fw-widget">
                          <thead>
                            <tr>
                              <th>NIS</th>
                              <th>Nama Siswa</th>
                              <th>Kelas</th>
                              <th>Tahun Masuk</th>
                              <th>Option</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
      load_data();
      var table1 =  $('#table1').DataTable();
      $('#form-input').submit(function(e){
        e.preventDefault();
        var form = $("#form-input").serialize();
        if ($('#kelas').val()=="") {
          alert('Kelas harus diisi');
          $('#kelas').focus();
        }else{
          $.ajax({
            type: "POST",
            url: "storeSiswa",
            data:  form,
            success: function(result){
              table1.ajax.reload();
              $.extend($.gritter.options,
                { position: 'bottom-left' }
              );
              $.gritter.add({
                title: 'Sukses!',
                text: 'Data Siswa baru sukses dimasukkan.',
                class_name: 'color success'
              });
              $('#nis').val("");
              $('#nama').val("");
              $('#nis').focus();
            }
          });
        }
        return false;
      });
    });
    </script>

    <script type="text/javascript">
      function load_data(){
        $('#table1').DataTable( {
            "order": [[ 0, "desc" ]],
            "ajax": {
                "url": "api/api/api_datasiswa",
                "dataSrc": ""
            },
            "columns": [
                { "data": "nis" },
                { "data": "nama_siswa" },
                { "data": "kelas" },
                { "data": "tahun" },
                {
                  "data": "nis",
                  "orderable": false,
                  "mRender": function (data) {
                    return '<a href="'+data+'"><button class="btn btn-rounded btn-space btn-primary"><i class="icon icon-left mdi mdi-edit"></i> Update</button></a> <a href="'+data+'"><button class="btn btn-rounded btn-space btn-danger"><i class="icon icon-left mdi mdi-delete"></i> Delete</button></a>';
                  }
                }
            ]
        } );
      }
    </script>
    <script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/main.js" type="text/javascript"></script>
    <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
    <script src="assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
    <script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="assets/lib/jquery.gritter/js/jquery.gritter.js" type="text/javascript"></script>
    <script src="assets/js/app-ui-notifications.js" type="text/javascript"></script>
    <script src="assets/js/app-form-elements.js" type="text/javascript"></script>
    <script src="assets/lib/select2/js/select2.min.js" type="text/javascript"></script>


    <script type="text/javascript">
    $(document).ready(function(){
      //initialize the javascript
      App.init();
      App.dataTables();
      App.uiNotifications();
      App.formElements();
    });
    </script>
  </body>
  </html>
