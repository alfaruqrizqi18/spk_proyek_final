<div class="panel-heading panel-heading-divider">Tabel Rating - Page rendered <strong>({elapsed_time})</strong></div>
<table id="table-first" class="table table-hover table-fw-widget">
  <thead>
    <tr>
      <th>Nomor</th>
      <th>Kelas</th>
      <th>Nama Siswa</th>
      <th>MAT</th>
      <th>FIS</th>
      <th>KIM</th>
      <th>BIO</th>
      <th>EKO</th>
      <th>SOS</th>
      <th>GEO</th>
      <th>MINAT IPA</th>
      <th>MINAT IPS</th>
      <th>PSIKOTEST</th>
    </tr>
  </thead>
  <tbody>
    <?php $nomor=1; ?>
    <?php foreach ($showTabelRating as $data): ?>
      <tr class="odd gradeX">
        <td><?php echo $nomor; ?></td>
        <td><?php echo "Kelas ".$data['kelas'] ?></td>
        <td><?php echo $data['nama_siswa'] ?></td>
        <td><?php echo $data['nilai_mat']?></td>
        <td><?php echo $data['nilai_fisika'] ?></td>
        <td><?php echo $data['nilai_kimia'] ?></td>
        <td><?php echo $data['nilai_biologi'] ?></td>
        <td><?php echo $data['nilai_ekonomi'] ?></td>
        <td><?php echo $data['nilai_sosiologi'] ?></td>
        <td><?php echo $data['nilai_geografi'] ?></td>
        <td><?php echo $data['nilai_minat_ipa'] ?></td>
        <td><?php echo $data['nilai_minat_ips'] ?></td>
        <td><?php echo $data['nilai_psikotest'] ?></td>
      </tr>
      <?php
      $nomor++;endforeach; ?>
    </tbody>
  </table><br>
  <script src="assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/pdfmake.min.js" charset="utf-8"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/vfs_fonts.js" charset="utf-8"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
  <script src="assets/js/app-tables-datatables.js" type="text/javascript"></script>


  <script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    App.init();
    $('#table-first').dataTable();
  });
  </script>
