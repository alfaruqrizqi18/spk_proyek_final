<div class="be-content">
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-border-color panel-border-color-primary">
          <div class="panel-body">
            <div class="col-sm-12">
              <div class="panel panel-default">
                <div class="tab-container">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#nilai" data-toggle="tab">Input Nilai</a></li>
                    <li><a href="#tab-rating" data-toggle="tab">Tabel Rating</a></li>
                    <li><a href="#tab-rating-after-accumulation-ipa" data-toggle="tab">Tabel Akumulasi IPA</a></li>
                    <li><a href="#tab-rating-after-accumulation-ips" data-toggle="tab">Tabel Akumulasi IPS</a></li>
                    <li><a href="#tab-rating-final" data-toggle="tab">Tabel Final</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="nilai" class="tab-pane active cont">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="panel panel-default panel-border-color panel-border-color-primary">
                            <div class="panel-heading panel-heading-divider">Form Pengisian Nilai<span class="panel-subtitle">Isi semua kolom nilai yang telah disediakan.</span></div>
                            <div class="panel-body">
                              <form id="form-nilai" >
                                <div class="panel-heading panel-heading-divider">Siswa</div>
                                <div class="col-md-12">
                                  <div class="form-group col-md-2">
                                    <label>1. Kelas</label>
                                    <select class="form-control" name="kelas" id="kelas" required="">
                                      <option value="">Pilih Kelas :</option>
                                      <option value="A">A</option>
                                      <option value="B">B</option>
                                      <option value="C">C</option>
                                      <option value="D">D</option>
                                      <option value="E">E</option>
                                      <option value="F">F</option>
                                      <option value="G">G</option>
                                      <option value="H">H</option>
                                      <option value="I">I</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-md-3">
                                    <label>2. Nama Siswa</label>
                                    <select class="form-control" name="nama" id="nama" required="">
                                      <option></option>
                                    </select>
                                  </div>
                                </div>
                                <div class="panel-heading panel-heading-divider">Nilai Akademis</div>
                                <div class="col-md-12">
                                  <div class="form-group col-md-2">
                                    <label>1. Matematika</label>
                                    <input type="number" placeholder="..." class="form-control" name="mat" id="mat" required="">
                                  </div>
                                  <div class="form-group col-md-2">
                                    <label>2. Fisika</label>
                                    <input type="number" placeholder="..." class="form-control" name="fis" id="fis" required="">
                                  </div>
                                  <div class="form-group col-md-2">
                                    <label>3. Kimia</label>
                                    <input type="number" placeholder="..." class="form-control" name="kim" id="kim" required="">
                                  </div>
                                  <div class="form-group col-md-2">
                                    <label>4. Biologi</label>
                                    <input type="number" placeholder="..." class="form-control" name="bio" id="bio" required="">
                                  </div>
                                  <div class="form-group col-md-2">
                                    <label>5. Ekonomi</label>
                                    <input type="number" placeholder="..." class="form-control" name="eko" id="eko" required="">
                                  </div>
                                  <div class="form-group col-md-2">
                                    <label>6. Sosiologi</label>
                                    <input type="number" placeholder="..." class="form-control" name="sos" id="sos" required="">
                                  </div>
                                  <div class="form-group col-md-2">
                                    <label>7. Geografi</label>
                                    <input type="number" placeholder="..." class="form-control" name="geo" id="geo" required="">
                                  </div>
                                </div>
                                <div class="panel-heading panel-heading-divider">Nilai Non-Akademis</div>
                                <div class="col-md-12">
                                  <div class="form-group col-md-2">
                                    <label>Minat Jurusan IPA</label>
                                    <select class="form-control" name="minat_ipa" id="minat_ipa" required="">
                                      <option value="">-------------</option>
                                      <option value="5">Sangat Minat</option>
                                      <option value="4">Minat</option>
                                      <option value="3">Cukup</option>
                                      <option value="2">Kurang Minat</option>
                                      <option value="1">Tidak Minat</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-md-2">
                                    <label>Minat Jurusan IPS</label>
                                    <select class="form-control" name="minat_ips" id="minat_ips" required="">
                                      <option value="">-------------</option>
                                      <option value="5">Sangat Minat</option>
                                      <option value="4">Minat</option>
                                      <option value="3">Cukup</option>
                                      <option value="2">Kurang Minat</option>
                                      <option value="1">Tidak Minat</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-md-2">
                                    <label>Psikotest</label>
                                    <input type="number" placeholder="..." class="form-control" name="psiko" id="psiko" required="">
                                  </div><br><br>
                                  <div class="form-group col-md-6">
                                      <button type="submit" class="btn btn-space btn-primary">Simpan sementara</button>
                                      <button type="reset" class="btn btn-space btn-default">Reset</button>
                                      <button class="btn btn-space btn-success" onclick="quick_count()">Penjurusan Siap dihitung!</button>
                                  </div>
                                    <p id="loading" style="alignment:center"><img src="assets/cube.gif" alt=""></p>
                                </div>
                                <div class="col-md-12">
                                  <div class="col-md-6">
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="tab-rating" class="tab-pane">
                      <div class="panel panel-default panel-table">
                        <div class="panel-body" >
                          <div class="col-sm-12">
                            <div class="panel panel-default panel-table">
                            <button type="button" onclick="onReloadTabelRating()" class="btn btn-space btn-primary">Segarkan Tabel</button>
                              <div class="panel-body" id="tabel-rating">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="tab-rating-after-accumulation-ipa" class="tab-pane">
                      <div class="panel panel-default panel-table">
                        <div class="panel-body" >
                          <div class="col-sm-12">
                            <div class="panel panel-default panel-table">
                            <button type="button" onclick="onReloadTabelRatingIPA()" class="btn btn-space btn-primary">Segarkan Tabel</button>
                              <div class="panel-body" id="tabel-rating-after-accumulation-ipa">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="tab-rating-after-accumulation-ips" class="tab-pane">
                      <div class="panel panel-default panel-table">
                        <div class="panel-body" >
                          <div class="col-sm-12">
                            <div class="panel panel-default panel-table">
                            <button type="button" onclick="onReloadTabelRatingIPS()" class="btn btn-space btn-primary">Segarkan Tabel</button>
                              <div class="panel-body" id="tabel-rating-after-accumulation-ips">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="tab-rating-final" class="tab-pane">
                      <div class="panel panel-default panel-table">
                        <div class="panel-body" >
                          <div class="col-sm-12">
                            <div class="panel panel-default panel-table">
                            <button type="button" onclick="onReloadTabelRatingFINAL()" class="btn btn-space btn-primary">Segarkan Tabel</button>
                              <div class="panel-body" id="tabel-rating-final">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script>
$(document).ready(function () {
    $('#kelas').change(function () {
        var kelas = $(this).val();
        console.log(kelas);  //menampilan pada log browser apa yang dibawa pada saat dipilih pada combo box
        $.ajax({
            url: "administrator/showSiswaByClass/",       //memanggil function controller dari url
            async: false,
            type: "POST",     //jenis method yang dibawa ke function
            data: "kelas="+kelas,   //data yang akan dibawa di url
            dataType: "html",
            success: function(data) {
                $('#nama').html(data);
                console.log(data)
                  //hasil ditampilkan pada combobox id=kota
            }
        })
    });
 });
  function onReloadTabelRating(){
   $('#tabel-rating').load('administrator/showTabelRating');
        return false;
  }
  function onReloadTabelRatingIPA(){
   $('#tabel-rating-after-accumulation-ipa').load('administrator/showTabelRatingIPA');
        return false;
  }
  function onReloadTabelRatingIPS(){
   $('#tabel-rating-after-accumulation-ips').load('administrator/showTabelRatingIPS');
        return false;
  }
  function onReloadTabelRatingFINAL(){
   var a = $('#tabel-rating-final').load('administrator/showTabelRatingFINAL');
        return false;
  }
 function onStart(){
   var kelas = $('#kelas').val();
   console.log(kelas);  //menampilan pada log browser apa yang dibawa pada saat dipilih pada combo box
   $.ajax({
       url: "administrator/showSiswaByClass/",       //memanggil function controller dari url
       async: false,
       type: "POST",     //jenis method yang dibawa ke function
       data: "kelas="+kelas,   //data yang akan dibawa di url
       dataType: "html",
       success: function(data) {
           $('#nama').html(data);   //hasil ditampilkan pada combobox id=kota
       }
   })
 }
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $("#loading").hide();
      $('#form-nilai').submit(function(e){
        e.preventDefault();
        var form = $("#form-nilai").serialize();
          $.ajax({
            type: "POST",
            url: "storeNilai",
            data:  form,
            beforeSend: function() {
            $("#loading").show();
            },
            success: function(result){
              onStart()
            $("#loading").hide();
              $('#mat').val(""); $('#fis').val(""); $('#kim').val(""); $('#bio').val(""); $('#eko').val(""); $('#sos').val(""); $('#geo').val("");
              $('#minat_ipa').val("");$('#minat_ips').val("");$('#psiko').val("");
              $('#mat').focus();
              $.extend($.gritter.options,
                { position: 'top-right' }
              );
              $.gritter.add({
                title: 'Sukses!',
                text: 'Data Nilai baru sukses dimasukkan.',
                class_name: 'color success'
              });
            }
          });
        return false;
      });
    });
    </script>
    <script type="text/javascript">
      function quick_count(){
        $.ajax({
          type: "POST",
          url: "quick_count",
          beforeSend: function() {
          $("#loading").show();
          },
          success: function(result){
          $("#loading").hide();
            $.extend($.gritter.options,
              { position: 'top-right' }
            );
            $.gritter.add({
              title: 'Selamat!',
              text: 'Penjurusan telah selesai.',
              class_name: 'color success'
            });
          }
        });
      }
    </script>
<script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>
<script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/lib/jquery.gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
  //initialize the javascript
  App.init();
  App.uiNotifications();
});
</script>
</body>
</html>
