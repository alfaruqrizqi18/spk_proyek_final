<div class="be-content">
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">Data Master User</div>
          <div class="panel-body">
            <form id="form-user">
              <div class="form-group col-md-3">
                <input type="text" name="nama" id="nama" parsley-trigger="change" required="" placeholder="Nama..." autocomplete="off" autofocus class="form-control">
              </div>
              <div class="form-group col-md-3">
                <input type="text" name="username" id="username" parsley-trigger="change" required="" placeholder="Username..." autocomplete="off" class="form-control">
              </div>
              <div class="form-group col-md-3">
                <input type="password" name="password" id="password" parsley-trigger="change" required="" placeholder="Password..." autocomplete="off" class="form-control">
              </div>
              <div class="col-md-2">
                <button type="submit" class="btn btn-space btn-success btn-xl">
                  <i class="icon icon-left mdi mdi-cloud-done"></i>
                  Tambahkan User</button>
                </div>
              </form>
            </div>
            <div class="panel panel-default panel-table">
              <div class="panel-body" >
                <div class="col-sm-12">
                  <div class="panel panel-default panel-table">
                    <div class="panel-body" id="tabel-user">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#tabel-user').load('administrator/tabelUser');
    $('#form-user').submit(function(e){
      e.preventDefault();
      var form = $("#form-user").serialize();
      if ($('#nama').val() == "" || $('#username').val() == ""|| $('#password').val() == "") {
        alert('Semua kolom harus diisi');
      }else{
        $.ajax({
          type: "POST",
          url: "storeUser",
          data:  form,
          success: function(result){
            $('#tabel-user').load('administrator/tabelUser');
            $.extend($.gritter.options,
              { position: 'bottom-left' }
            );
            $.gritter.add({
              title: 'Sukses!',
              text: 'Data User baru sukses dimasukkan.',
              class_name: 'color success'
            });
            $('#nama').val("");
            $('#username').val("");
            $('#password').val("");
            $('#nama').focus();
          }
        });
      }
      return false;
    });
  });
  </script>
  <script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
  <script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/main.js" type="text/javascript"></script>
  <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <script src="assets/lib/jquery.gritter/js/jquery.gritter.js" type="text/javascript"></script>
  <script src="assets/js/app-ui-notifications.js" type="text/javascript"></script>
  <script src="assets/js/app-form-elements.js" type="text/javascript"></script>
  <script src="assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    App.init();
    App.uiNotifications();
    App.formElements();
  });
  </script>
</body>
</html>
