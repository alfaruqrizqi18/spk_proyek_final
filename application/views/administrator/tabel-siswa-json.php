<table id="table1" class="table table-hover table-fw-widget">
  <thead>
    <tr>
      <th>NIS</th>
      <th>Nama Siswa</th>
      <th>Kelas</th>
      <th>Tahun Masuk</th>
      <th>Option</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
<script type="text/javascript">
$(document).ready(function() {
    $('#table1').DataTable( {
        "ajax": {
            "url": "api/api/api_datasiswa",
            "dataSrc": ""
        },
        "columns": [
            { "data": "nis" },
            { "data": "nama_siswa" },
            { "data": "kelas" },
            { "data": "tahun" }
        ]
    } );
} );
</script>
<script src="assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
<script src="assets/js/app-tables-datatables.js" type="text/javascript"></script>


<script type="text/javascript">
$(document).ready(function(){
  //initialize the javascript
  App.init();
  App.dataTables();
});
</script>
