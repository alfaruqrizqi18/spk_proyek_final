<div class="be-content">
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-border-color panel-border-color-primary">
          <div class="panel-heading"><h1 class="text-center">Selamat datang Administrator</h1></div>
          <div class="panel-body">
            <blockquote>
              <p>Sistem Pendukung Keputusan Penjurusan adalah sebuah aplikasi yang dibangun dengan
                tujuan dapat membantu pihak Bimbingan Konseling (BK) dan
                Akademis-Kurikulum dalam menentukan seorang siswa akan masuk jurusan mana,
                sesuai kriteria yang telah disediakan. </p>
                <footer>Muhammad Rizqi Alfaruq di
                  <cite title="Source Title"> Politeknik Kediri</cite>
                </footer>
              </blockquote>
              <p class="xs-mt-10 xs-mb-10 text-center">
                <a href="firstStep"><button class="btn btn-space btn-primary btn-lg"><i class="icon icon-left mdi mdi-cloud-done"></i> Buat Penjurusan sekarang!</button></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>
<script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/lib/prettify/prettify.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
  //initialize the javascript
  App.init();

  //Runs prettify
  prettyPrint();
});
</script>
</body>
</html>
