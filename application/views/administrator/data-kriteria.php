<div class="be-content">
  <div class="main-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">Data Master Kriteria</div>
          <div class="panel-body">
            <!-- <form id="form-kriteria">
              <div class="form-group col-md-3">
                <input type="text" name="kriteria" id="kriteria" parsley-trigger="change" required="" placeholder="Nama Kriteria..." autocomplete="off" autofocus class="form-control">
              </div>
              <div class="form-group col-md-2">
                <select class="form-control" name="bobot_ipa" id="bobot_ipa">
                  <option value="">Bobot IPA : </option>
                  <option value="1">Sangat Baik</option>
                  <option value="0.8">Baik</option>
                  <option value="0.6">Cukup</option>
                  <option value="0.4">Kurang</option>
                  <option value="0.2">Sangat Kurang</option>
                </select>
              </div>
              <div class="form-group col-md-2">
                <select class="form-control" name="bobot_ips" id="bobot_ips">
                  <option value="">Bobot IPS : </option>
                  <option value="1">Sangat Baik</option>
                  <option value="0.8">Baik</option>
                  <option value="0.6">Cukup</option>
                  <option value="0.4">Kurang</option>
                  <option value="0.2">Sangat Kurang</option>
                </select>
              </div>
              <div class="col-md-2">
                <button type="submit" class="btn btn-space btn-success btn-xl">
                  <i class="icon icon-left mdi mdi-cloud-done"></i>
                  Tambahkan Kriteria</button>
                </div>
              </form> -->
            </div>
            <div class="col-md-3">
              <div role="alert" class="alert alert-contrast alert-danger alert-dismissible">
                <div class="message">
                  <strong>Sangat Kurang : </strong> 50 - 60
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div role="alert" class="alert alert-contrast alert-warning alert-dismissible">
                <div class="message">
                  <strong>Kurang : </strong> 61 - 70
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                <div class="message">
                  <strong>Cukup : </strong> 71 - 80
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div role="alert" class="alert alert-contrast alert-success alert-dismissible">
                <div class="message">
                  <strong>Baik : </strong> 81 - 90
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div role="alert" class="alert alert-contrast alert-success alert-dismissible">
                <div class="message">
                  <strong>Sangat Baik : </strong> 91 - 100
                </div>
              </div>
            </div>
            <div class="panel panel-default panel-table">
              <div class="panel-body" >
                <div class="col-sm-12">
                  <div class="panel panel-default panel-table">
                    <div class="panel-body" id="tabel-kriteria">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  $(document).ready(function(){
    $('#tabel-kriteria').load('administrator/tabelKriteria');
    $('#form-kriteria').submit(function(e){
      e.preventDefault();
      var form = $("#form-kriteria").serialize();
      if ($('#kriteria').val() == "" || $('#bobot_ipa').val() == "" || $('#bobot_ips').val() == "") {
        alert('Semua kolom harus diisi');
      }else{
        $.ajax({
          type: "POST",
          url: "storeKriteria",
          data:  form,
          success: function(result){
            $('#tabel-kriteria').load('administrator/tabelKriteria');
            $.extend($.gritter.options,
              { position: 'bottom-left' }
            );
            $.gritter.add({
              title: 'Sukses!',
              text: 'Data Kriteria baru sukses dimasukkan.',
              class_name: 'color success'
            });
            $('#kriteria').val("");
            $('#bobot_ipa').val("");
            $('#bobot_ips').val("");
            $('#kriteria').focus();
          }
        });
      }
      return false;
    });
  });
  </script>
  <script src="assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
  <script src="assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
  <script src="assets/js/main.js" type="text/javascript"></script>
  <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <script src="assets/lib/jquery.gritter/js/jquery.gritter.js" type="text/javascript"></script>
  <script src="assets/js/app-ui-notifications.js" type="text/javascript"></script>
  <script src="assets/js/app-form-elements.js" type="text/javascript"></script>
  <script src="assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    App.init();
    App.uiNotifications();
    App.formElements();
  });
  </script>
</body>
</html>
