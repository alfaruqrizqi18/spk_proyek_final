<div class="panel-heading panel-heading-divider">Tabel Rating Setelah di Kalkulasi ((Rating / Nilai Maksimal per Kolom) * Bobot per Kriteria) -> IPS - Page rendered <strong>({elapsed_time})</strong></div>
<table id="table-ips" class="table table-hover table-fw-widget">
  <thead>
    <tr>
      <th>Nomor</th>
      <th>Kelas</th>
      <th>Nama Siswa</th>
      <th>MAT</th>
      <th>FIS</th>
      <th>KIM</th>
      <th>BIO</th>
      <th>EKO</th>
      <th>SOS</th>
      <th>GEO</th>
      <th>MINAT IPA</th>
      <th>MINAT IPS</th>
      <th>PSIKOTEST</th>
      <th>Hasil Sementara</th>
    </tr>
  </thead>
  <tbody>
    <?php $nomor=1; ?>
    <?php foreach ($showTabelRating as $data): ?>
      <tr class="odd gradeX">
        <td><?php echo $nomor; ?></td>
        <td><?php echo "Kelas ".$data['kelas'] ?></td>
        <td><?php echo $data['nama_siswa'] ?></td>

        <?php foreach ($showMaxRating as $data_max): ?>
          <?php foreach ($showKriteriaMATforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_mat']/$data_max['max_mat'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")" ?></td>
            <?php $mat2 = ($data['nilai_mat']/$data_max['max_mat'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>

          <?php foreach ($showKriteriaFISforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_fisika']/$data_max['max_fis'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")"; ?></td>
            <?php $fis2 = ($data['nilai_fisika']/$data_max['max_fis'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>

          <?php foreach ($showKriteriaKIMforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_kimia']/$data_max['max_kim'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")"; ?></td>
            <?php $kim2 = ($data['nilai_kimia']/$data_max['max_kim'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>

          <?php foreach ($showKriteriaBIOforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_biologi']/$data_max['max_bio'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")"; ?></td>
            <?php $bio2 = ($data['nilai_biologi']/$data_max['max_bio'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>

          <?php foreach ($showKriteriaEKOforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_ekonomi']/$data_max['max_eko'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")"; ?></td>
            <?php $eko2 = ($data['nilai_ekonomi']/$data_max['max_eko'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>

          <?php foreach ($showKriteriaSOSforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_sosiologi']/$data_max['max_sos'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")"; ?></td>
            <?php $sos2 = ($data['nilai_sosiologi']/$data_max['max_sos'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>

          <?php foreach ($showKriteriaGEOforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_geografi']/$data_max['max_geo'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")"; ?></td>
            <?php $geo2 = ($data['nilai_geografi']/$data_max['max_geo'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>

          <?php foreach ($showKriteriaMINAT_IPAforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_minat_ipa']/$data_max['max_ipa'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")"; ?></td>
            <?php $ipa2 = ($data['nilai_minat_ipa']/$data_max['max_ipa'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>

          <?php foreach ($showKriteriaMINAT_IPSforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_minat_ips']/$data_max['max_ips'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")"; ?></td>
            <?php $ips2 = ($data['nilai_minat_ips']/$data_max['max_ips'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>

          <?php foreach ($showKriteriaPSIKOforIPS as $data_kriteria_ips): ?>
            <td><?php echo number_format($data['nilai_psikotest']/$data_max['max_psiko'],2)*$data_kriteria_ips['bobot_IPS']."(".$data_kriteria_ips['bobot_IPS'].")"; ?></td>
            <?php $psiko2 = ($data['nilai_psikotest']/$data_max['max_psiko'])*$data_kriteria_ips['bobot_IPS']; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
        <td><?php echo number_format($mat2+$fis2+$kim2+$bio2+$eko2+$sos2+$geo2+$ipa2+$ips2+$psiko2,2); ?></td>
      </tr>
      <?php
      $nomor++;endforeach; ?>
    </tbody>
  </table>
  <script src="assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/pdfmake.min.js" charset="utf-8"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/vfs_fonts.js" charset="utf-8"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
  <script src="assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
  <script src="assets/js/app-tables-datatables.js" type="text/javascript"></script>


  <script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    App.init();
    $("#table-ips").dataTable();
  });
  </script>
