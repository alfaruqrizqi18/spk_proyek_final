<table id="table1" class="table table-hover table-fw-widget">
  <thead>
    <tr>
      <th>NIS</th>
      <th>Nama Siswa</th>
      <th>Kelas</th>
      <th>Tahun Masuk</th>
      <th>Option</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($showSiswa as $data): ?>
    <tr class="odd gradeX">
      <td><?php echo $data['nis']; ?></td>
      <td><?php echo $data['nama_siswa']; ?></td>
      <td><?php echo $data['kelas']; ?></td>
      <td><?php echo $data['tahun']; ?></td>
      <td><a href="#"><button class="btn btn-rounded btn-space btn-primary">
        <i class="icon icon-left mdi mdi-edit"></i> Update
      </button></a><button class="btn btn-rounded btn-space btn-danger">
        <i class="icon icon-left mdi mdi-delete"></i> Hapus
        </button></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<script src="assets/lib/datatables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="assets/lib/datatables/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.html5.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.flash.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.print.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.colVis.js" type="text/javascript"></script>
<script src="assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js" type="text/javascript"></script>
<script src="assets/js/app-tables-datatables.js" type="text/javascript"></script>


<script type="text/javascript">
$(document).ready(function(){
  //initialize the javascript
  App.init();
  App.dataTables();
});
</script>
