$(document).ready(function(){
  $('#tabel-siswa').load('administrator/tabelSiswa');
  $('#form-input').submit(function(e){
    e.preventDefault();
    var form = $("#form-input").serialize();
    $.ajax({
      type: "POST",
      url: "administrator/storeSiswa/",
      data:  form,
      success: function(result){
        $('#tabel-siswa').load('administrator/tabelSiswa');
        $.extend($.gritter.options, { position: 'bottom-right' });
        $.gritter.add({
          title: 'Sukses!',
          text: 'Data Siswa baru sukses dimasukkan.',
          class_name: 'color success'
        });
        $('#nis').val("");
        $('#nama').val("");
        $('#nis').focus();
      }
    });
    return false;
  });
});
