-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 03 Jul 2017 pada 09.44
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spk_proyek`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `spk_datakriteria`
--

CREATE TABLE `spk_datakriteria` (
  `id_kriteria` int(3) NOT NULL,
  `nama_kriteria` varchar(40) NOT NULL,
  `bobot_IPA` float NOT NULL,
  `bobot_IPS` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spk_datakriteria`
--

INSERT INTO `spk_datakriteria` (`id_kriteria`, `nama_kriteria`, `bobot_IPA`, `bobot_IPS`) VALUES
(1, 'Matematika', 0.6, 0.4),
(2, 'Fisika', 0.6, 0.4),
(3, 'Kimia', 0.6, 0.4),
(4, 'Biologi', 0.6, 0.4),
(5, 'Ekonomi', 0.4, 0.6),
(6, 'Sosiologi', 0.4, 0.6),
(7, 'Geografi', 0.4, 0.6),
(8, 'Minat di Jurusan IPA', 1, 0.4),
(9, 'Minat di Jurusan IPS', 0.4, 1),
(10, 'Psikotest', 0.8, 0.8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `spk_datanilai`
--

CREATE TABLE `spk_datanilai` (
  `id_nilai` int(11) NOT NULL,
  `nis` int(11) NOT NULL,
  `nilai_mat` float NOT NULL,
  `nilai_fisika` float NOT NULL,
  `nilai_kimia` float NOT NULL,
  `nilai_biologi` float NOT NULL,
  `nilai_ekonomi` float NOT NULL,
  `nilai_sosiologi` float NOT NULL,
  `nilai_geografi` float NOT NULL,
  `minat_ipa` float NOT NULL,
  `minat_ips` float NOT NULL,
  `nilai_psikotest` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spk_datanilai`
--

INSERT INTO `spk_datanilai` (`id_nilai`, `nis`, `nilai_mat`, `nilai_fisika`, `nilai_kimia`, `nilai_biologi`, `nilai_ekonomi`, `nilai_sosiologi`, `nilai_geografi`, `minat_ipa`, `minat_ips`, `nilai_psikotest`) VALUES
(100, 15010086, 85, 90, 88, 70, 80, 90, 90, 3, 5, 90),
(101, 15010087, 90, 80, 78, 78, 80, 90, 90, 3, 4, 80),
(102, 15010088, 96, 96, 96, 96, 96, 96, 100, 1, 5, 80),
(103, 15010089, 85, 88, 96, 75, 85, 99, 99, 4, 2, 80),
(104, 15010090, 85, 88, 88, 88, 70, 90, 95, 5, 5, 85),
(105, 15010091, 65, 68, 78, 75, 85, 90, 90, 5, 5, 75),
(106, 15010092, 78, 88, 99, 99, 99, 100, 58, 4, 5, 86),
(107, 15010093, 85, 96, 77, 77, 85, 99, 99, 5, 3, 85),
(108, 15010094, 85, 86, 85, 78, 58, 99, 99, 5, 5, 58),
(109, 15010095, 45, 75, 78, 75, 74, 74, 85, 4, 4, 85),
(110, 15010096, 85, 96, 78, 88, 88, 90, 95, 5, 5, 85),
(111, 15010097, 45, 85, 99, 99, 85, 96, 96, 4, 5, 85),
(112, 15010098, 85, 96, 96, 78, 78, 96, 96, 3, 4, 85),
(113, 15010099, 53, 85, 85, 75, 96, 95, 85, 3, 4, 85),
(114, 15010100, 75, 85, 77, 88, 75, 85, 99, 4, 5, 85),
(115, 15010101, 45, 85, 99, 66, 88, 59, 85, 4, 5, 78),
(116, 15010102, 78, 78, 85, 87, 99, 98, 87, 4, 5, 77),
(117, 15010103, 78, 89, 78, 85, 85, 96, 99, 5, 3, 77),
(118, 15010104, 90, 90, 90, 90, 90, 95, 96, 4, 5, 89),
(119, 15010105, 77, 85, 89, 78, 95, 68, 52, 4, 5, 75),
(120, 15010106, 75, 85, 98, 78, 58, 77, 55, 5, 5, 96),
(121, 15010107, 85, 96, 78, 52, 36, 87, 94, 5, 5, 78),
(122, 15010108, 85, 75, 78, 96, 88, 78, 58, 4, 1, 85),
(123, 15010109, 85, 96, 78, 85, 78, 77, 89, 4, 5, 99),
(124, 15010110, 85, 96, 78, 78, 85, 45, 89, 2, 3, 85),
(125, 15010111, 85, 96, 96, 96, 96, 96, 96, 4, 5, 85),
(126, 15010112, 75, 85, 96, 78, 45, 78, 88, 5, 4, 85),
(127, 15010113, 85, 96, 87, 88, 88, 60, 60, 3, 5, 85),
(128, 15010114, 45, 45, 45, 54, 45, 45, 45, 4, 5, 85),
(129, 15010115, 68, 96, 96, 96, 96, 96, 96, 5, 5, 96),
(130, 15010116, 90, 89, 78, 67, 89, 68, 90, 4, 5, 90),
(131, 15010117, 80, 88, 87, 70, 77, 77, 78, 5, 5, 85),
(132, 15010118, 85, 96, 88, 77, 78, 85, 85, 4, 5, 85);

-- --------------------------------------------------------

--
-- Struktur dari tabel `spk_datarating`
--

CREATE TABLE `spk_datarating` (
  `id_rating` int(11) NOT NULL,
  `nis` int(11) NOT NULL,
  `nilai_mat` float NOT NULL,
  `nilai_fisika` float NOT NULL,
  `nilai_kimia` float NOT NULL,
  `nilai_biologi` float NOT NULL,
  `nilai_ekonomi` float NOT NULL,
  `nilai_sosiologi` float NOT NULL,
  `nilai_geografi` float NOT NULL,
  `nilai_minat_ipa` float NOT NULL,
  `nilai_minat_ips` float NOT NULL,
  `nilai_psikotest` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spk_datarating`
--

INSERT INTO `spk_datarating` (`id_rating`, `nis`, `nilai_mat`, `nilai_fisika`, `nilai_kimia`, `nilai_biologi`, `nilai_ekonomi`, `nilai_sosiologi`, `nilai_geografi`, `nilai_minat_ipa`, `nilai_minat_ips`, `nilai_psikotest`) VALUES
(110, 15010086, 4, 4, 4, 2, 3, 4, 4, 3, 5, 3),
(111, 15010087, 4, 3, 3, 3, 3, 4, 4, 3, 4, 2),
(112, 15010088, 5, 5, 5, 5, 5, 5, 5, 1, 5, 2),
(113, 15010089, 4, 4, 5, 3, 4, 5, 5, 4, 2, 2),
(114, 15010090, 4, 4, 4, 4, 2, 4, 5, 5, 5, 2),
(115, 15010091, 2, 2, 3, 3, 4, 4, 4, 5, 5, 1),
(116, 15010092, 3, 4, 5, 5, 5, 5, 1, 4, 5, 2),
(117, 15010093, 4, 5, 3, 3, 4, 5, 5, 5, 3, 2),
(118, 15010094, 4, 4, 4, 3, 1, 5, 5, 5, 5, 1),
(119, 15010095, 1, 3, 3, 3, 3, 3, 4, 4, 4, 2),
(120, 15010096, 4, 5, 3, 4, 4, 4, 5, 5, 5, 2),
(121, 15010097, 1, 4, 5, 5, 4, 5, 5, 4, 5, 2),
(122, 15010098, 4, 5, 5, 3, 3, 5, 5, 3, 4, 2),
(123, 15010099, 1, 4, 4, 3, 5, 5, 4, 3, 4, 2),
(124, 15010100, 3, 4, 3, 4, 3, 4, 5, 4, 5, 2),
(125, 15010101, 1, 4, 5, 2, 4, 1, 4, 4, 5, 1),
(126, 15010102, 3, 3, 4, 4, 5, 5, 4, 4, 5, 1),
(127, 15010103, 3, 4, 3, 4, 4, 5, 5, 5, 3, 1),
(128, 15010104, 4, 4, 4, 4, 4, 5, 5, 4, 5, 2),
(129, 15010105, 3, 4, 4, 3, 5, 2, 1, 4, 5, 1),
(130, 15010106, 3, 4, 5, 3, 1, 3, 1, 5, 5, 3),
(131, 15010107, 4, 5, 3, 1, 1, 4, 5, 5, 5, 1),
(132, 15010108, 4, 3, 3, 5, 4, 3, 1, 4, 1, 2),
(133, 15010109, 4, 5, 3, 4, 3, 3, 4, 4, 5, 3),
(134, 15010110, 4, 5, 3, 3, 4, 1, 4, 2, 3, 2),
(135, 15010111, 4, 5, 5, 5, 5, 5, 5, 4, 5, 2),
(136, 15010112, 3, 4, 5, 3, 1, 3, 4, 5, 4, 2),
(137, 15010113, 4, 5, 4, 4, 4, 1, 1, 3, 5, 2),
(138, 15010114, 1, 1, 1, 1, 1, 1, 1, 4, 5, 2),
(139, 15010115, 2, 5, 5, 5, 5, 5, 5, 5, 5, 3),
(140, 15010116, 4, 4, 3, 2, 4, 2, 4, 4, 5, 3),
(141, 15010117, 3, 4, 4, 2, 3, 3, 3, 5, 5, 2),
(142, 15010118, 4, 5, 4, 3, 3, 4, 4, 4, 5, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `spk_datasiswa`
--

CREATE TABLE `spk_datasiswa` (
  `nis` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `tahun` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spk_datasiswa`
--

INSERT INTO `spk_datasiswa` (`nis`, `nama_siswa`, `kelas`, `tahun`) VALUES
(15010086, 'Muhammad Rizqi Alfaruq', 'A', '2017'),
(15010087, 'Rizqi', 'A', '2016'),
(15010088, 'Sumidi2', 'A', '2016'),
(15010089, 'Raikage', 'A', '2016'),
(15010090, 'Karlitos', 'A', '2016'),
(15010091, 'Fuyuki', 'A', '2016'),
(15010092, 'Kempir', 'A', '2016'),
(15010093, 'Rizal', 'A', '2016'),
(15010094, 'Kamto', 'A', '2016'),
(15010095, 'Arfian', 'A', '2016'),
(15010096, 'Lea', 'A', '2016'),
(15010097, 'Phil', 'A', '2016'),
(15010098, 'Koko', 'A', '2016'),
(15010099, 'Daisuki', 'A', '2017'),
(15010100, 'Meme', 'A', '2016'),
(15010101, 'Flare', 'A', '2016'),
(15010102, 'Sis', 'A', '2016'),
(15010103, 'Plea', 'A', '2016'),
(15010104, 'Sop', 'A', '2016'),
(15010105, 'Polls', 'A', '2016'),
(15010106, 'Plisz', 'A', '2016'),
(15010107, 'Realisz', 'A', '2016'),
(15010108, 'Learizel', 'A', '2015'),
(15010109, 'Learizel 2', 'A', '2016'),
(15010110, 'Flapp', 'A', '2016'),
(15010111, 'Neko', 'A', '2016'),
(15010112, 'Poling', 'A', '2016'),
(15010113, 'Flash', 'A', '2016'),
(15010114, 'Kleo', 'A', '2016'),
(15010115, 'Flew', 'A', '2016'),
(15010116, 'Patrick', 'A', '2016'),
(15010117, 'Tux', 'A', '2017'),
(15010118, 'Dut', 'A', '2017');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spk_datauser`
--

CREATE TABLE `spk_datauser` (
  `id_user` int(5) NOT NULL,
  `nama_user` varchar(60) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spk_datauser`
--

INSERT INTO `spk_datauser` (`id_user`, `nama_user`, `username`, `password`, `level`) VALUES
(1, 'Administrator', 'admin', '123', 'SPK-ADMIN'),
(2, 'Sumidi', 'sumidi', '123', 'SPK-ADMIN'),
(3, 'Clone', 'clone', '123', 'SPK-ADMIN'),
(4, 'Phil Johnson', 'phil', '123', 'SPK-ADMIN');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spk_finalipa`
--

CREATE TABLE `spk_finalipa` (
  `id_nilai_final_ipa` int(11) NOT NULL,
  `nis` int(11) NOT NULL,
  `nilai_mat` float NOT NULL,
  `nilai_fisika` float NOT NULL,
  `nilai_kimia` float NOT NULL,
  `nilai_biologi` float NOT NULL,
  `nilai_ekonomi` float NOT NULL,
  `nilai_sosiologi` float NOT NULL,
  `nilai_geografi` float NOT NULL,
  `nilai_minat_ipa` float NOT NULL,
  `nilai_minat_ips` float NOT NULL,
  `nilai_psikotest` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spk_finalipa`
--

INSERT INTO `spk_finalipa` (`id_nilai_final_ipa`, `nis`, `nilai_mat`, `nilai_fisika`, `nilai_kimia`, `nilai_biologi`, `nilai_ekonomi`, `nilai_sosiologi`, `nilai_geografi`, `nilai_minat_ipa`, `nilai_minat_ips`, `nilai_psikotest`) VALUES
(1, 15010118, 0.48, 0.6, 0.48, 0.36, 0.24, 0.32, 0.32, 0.4, 0.4, 0.53),
(2, 15010117, 0.36, 0.48, 0.48, 0.24, 0.24, 0.24, 0.24, 0.4, 0.4, 0.53),
(3, 15010116, 0.48, 0.48, 0.36, 0.24, 0.32, 0.16, 0.32, 0.4, 0.4, 0.8),
(4, 15010115, 0.24, 0.6, 0.6, 0.6, 0.4, 0.4, 0.4, 0.4, 0.4, 0.8),
(5, 15010114, 0.12, 0.12, 0.12, 0.12, 0.08, 0.08, 0.08, 0.4, 0.4, 0.53),
(6, 15010113, 0.48, 0.6, 0.48, 0.48, 0.32, 0.08, 0.08, 0.4, 0.4, 0.53),
(7, 15010112, 0.36, 0.48, 0.6, 0.36, 0.08, 0.24, 0.32, 0.32, 0.32, 0.53),
(8, 15010111, 0.48, 0.6, 0.6, 0.6, 0.4, 0.4, 0.4, 0.4, 0.4, 0.53),
(9, 15010110, 0.48, 0.6, 0.36, 0.36, 0.32, 0.08, 0.32, 0.24, 0.24, 0.53),
(10, 15010109, 0.48, 0.6, 0.36, 0.48, 0.24, 0.24, 0.32, 0.4, 0.4, 0.8),
(11, 15010108, 0.48, 0.36, 0.36, 0.6, 0.32, 0.24, 0.08, 0.08, 0.08, 0.53),
(12, 15010107, 0.48, 0.6, 0.36, 0.12, 0.08, 0.32, 0.4, 0.4, 0.4, 0.27),
(13, 15010106, 0.36, 0.48, 0.6, 0.36, 0.08, 0.24, 0.08, 0.4, 0.4, 0.8),
(14, 15010105, 0.36, 0.48, 0.48, 0.36, 0.4, 0.16, 0.08, 0.4, 0.4, 0.27),
(15, 15010104, 0.48, 0.48, 0.48, 0.48, 0.32, 0.4, 0.4, 0.4, 0.4, 0.53),
(16, 15010103, 0.36, 0.48, 0.36, 0.48, 0.32, 0.4, 0.4, 0.24, 0.24, 0.27),
(17, 15010102, 0.36, 0.36, 0.48, 0.48, 0.4, 0.4, 0.32, 0.4, 0.4, 0.27),
(18, 15010101, 0.12, 0.48, 0.6, 0.24, 0.32, 0.08, 0.32, 0.4, 0.4, 0.27),
(19, 15010100, 0.36, 0.48, 0.36, 0.48, 0.24, 0.32, 0.4, 0.4, 0.4, 0.53),
(20, 15010099, 0.12, 0.48, 0.48, 0.36, 0.4, 0.4, 0.32, 0.32, 0.32, 0.53),
(21, 15010098, 0.48, 0.6, 0.6, 0.36, 0.24, 0.4, 0.4, 0.32, 0.32, 0.53),
(22, 15010097, 0.12, 0.48, 0.6, 0.6, 0.32, 0.4, 0.4, 0.4, 0.4, 0.53),
(23, 15010096, 0.48, 0.6, 0.36, 0.48, 0.32, 0.32, 0.4, 0.4, 0.4, 0.53),
(24, 15010095, 0.12, 0.36, 0.36, 0.36, 0.24, 0.24, 0.32, 0.32, 0.32, 0.53),
(25, 15010094, 0.48, 0.48, 0.48, 0.36, 0.08, 0.4, 0.4, 0.4, 0.4, 0.27),
(26, 15010093, 0.48, 0.6, 0.36, 0.36, 0.32, 0.4, 0.4, 0.24, 0.24, 0.53),
(27, 15010092, 0.36, 0.48, 0.6, 0.6, 0.4, 0.4, 0.08, 0.4, 0.4, 0.53),
(28, 15010091, 0.24, 0.24, 0.36, 0.36, 0.32, 0.32, 0.32, 0.4, 0.4, 0.27),
(29, 15010090, 0.48, 0.48, 0.48, 0.48, 0.16, 0.32, 0.4, 0.4, 0.4, 0.53),
(30, 15010089, 0.48, 0.48, 0.6, 0.36, 0.32, 0.4, 0.4, 0.16, 0.16, 0.53),
(31, 15010088, 0.6, 0.6, 0.6, 0.6, 0.4, 0.4, 0.4, 0.4, 0.4, 0.53),
(32, 15010087, 0.48, 0.36, 0.36, 0.36, 0.24, 0.32, 0.32, 0.32, 0.32, 0.53),
(33, 15010086, 0.48, 0.48, 0.48, 0.24, 0.24, 0.32, 0.32, 0.4, 0.4, 0.8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `spk_finalips`
--

CREATE TABLE `spk_finalips` (
  `id_nilai_final_ips` int(11) NOT NULL,
  `nis` int(11) NOT NULL,
  `nilai_mat` float NOT NULL,
  `nilai_fisika` float NOT NULL,
  `nilai_kimia` float NOT NULL,
  `nilai_biologi` float NOT NULL,
  `nilai_ekonomi` float NOT NULL,
  `nilai_sosiologi` float NOT NULL,
  `nilai_geografi` float NOT NULL,
  `nilai_minat_ipa` float NOT NULL,
  `nilai_minat_ips` float NOT NULL,
  `nilai_psikotest` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spk_finalips`
--

INSERT INTO `spk_finalips` (`id_nilai_final_ips`, `nis`, `nilai_mat`, `nilai_fisika`, `nilai_kimia`, `nilai_biologi`, `nilai_ekonomi`, `nilai_sosiologi`, `nilai_geografi`, `nilai_minat_ipa`, `nilai_minat_ips`, `nilai_psikotest`) VALUES
(1, 15010118, 0.32, 0.4, 0.32, 0.24, 0.36, 0.48, 0.48, 1, 1, 0.53),
(2, 15010117, 0.24, 0.32, 0.32, 0.16, 0.36, 0.36, 0.36, 1, 1, 0.53),
(3, 15010116, 0.32, 0.32, 0.24, 0.16, 0.48, 0.24, 0.48, 1, 1, 0.8),
(4, 15010115, 0.16, 0.4, 0.4, 0.4, 0.6, 0.6, 0.6, 1, 1, 0.8),
(5, 15010114, 0.08, 0.08, 0.08, 0.08, 0.12, 0.12, 0.12, 1, 1, 0.53),
(6, 15010113, 0.32, 0.4, 0.32, 0.32, 0.48, 0.12, 0.12, 1, 1, 0.53),
(7, 15010112, 0.24, 0.32, 0.4, 0.24, 0.12, 0.36, 0.48, 0.8, 0.8, 0.53),
(8, 15010111, 0.32, 0.4, 0.4, 0.4, 0.6, 0.6, 0.6, 1, 1, 0.53),
(9, 15010110, 0.32, 0.4, 0.24, 0.24, 0.48, 0.12, 0.48, 0.6, 0.6, 0.53),
(10, 15010109, 0.32, 0.4, 0.24, 0.32, 0.36, 0.36, 0.48, 1, 1, 0.8),
(11, 15010108, 0.32, 0.24, 0.24, 0.4, 0.48, 0.36, 0.12, 0.2, 0.2, 0.53),
(12, 15010107, 0.32, 0.4, 0.24, 0.08, 0.12, 0.48, 0.6, 1, 1, 0.27),
(13, 15010106, 0.24, 0.32, 0.4, 0.24, 0.12, 0.36, 0.12, 1, 1, 0.8),
(14, 15010105, 0.24, 0.32, 0.32, 0.24, 0.6, 0.24, 0.12, 1, 1, 0.27),
(15, 15010104, 0.32, 0.32, 0.32, 0.32, 0.48, 0.6, 0.6, 1, 1, 0.53),
(16, 15010103, 0.24, 0.32, 0.24, 0.32, 0.48, 0.6, 0.6, 0.6, 0.6, 0.27),
(17, 15010102, 0.24, 0.24, 0.32, 0.32, 0.6, 0.6, 0.48, 1, 1, 0.27),
(18, 15010101, 0.08, 0.32, 0.4, 0.16, 0.48, 0.12, 0.48, 1, 1, 0.27),
(19, 15010100, 0.24, 0.32, 0.24, 0.32, 0.36, 0.48, 0.6, 1, 1, 0.53),
(20, 15010099, 0.08, 0.32, 0.32, 0.24, 0.6, 0.6, 0.48, 0.8, 0.8, 0.53),
(21, 15010098, 0.32, 0.4, 0.4, 0.24, 0.36, 0.6, 0.6, 0.8, 0.8, 0.53),
(22, 15010097, 0.08, 0.32, 0.4, 0.4, 0.48, 0.6, 0.6, 1, 1, 0.53),
(23, 15010096, 0.32, 0.4, 0.24, 0.32, 0.48, 0.48, 0.6, 1, 1, 0.53),
(24, 15010095, 0.08, 0.24, 0.24, 0.24, 0.36, 0.36, 0.48, 0.8, 0.8, 0.53),
(25, 15010094, 0.32, 0.32, 0.32, 0.24, 0.12, 0.6, 0.6, 1, 1, 0.27),
(26, 15010093, 0.32, 0.4, 0.24, 0.24, 0.48, 0.6, 0.6, 0.6, 0.6, 0.53),
(27, 15010092, 0.24, 0.32, 0.4, 0.4, 0.6, 0.6, 0.12, 1, 1, 0.53),
(28, 15010091, 0.16, 0.16, 0.24, 0.24, 0.48, 0.48, 0.48, 1, 1, 0.27),
(29, 15010090, 0.32, 0.32, 0.32, 0.32, 0.24, 0.48, 0.6, 1, 1, 0.53),
(30, 15010089, 0.32, 0.32, 0.4, 0.24, 0.48, 0.6, 0.6, 0.4, 0.4, 0.53),
(31, 15010088, 0.4, 0.4, 0.4, 0.4, 0.6, 0.6, 0.6, 1, 1, 0.53),
(32, 15010087, 0.32, 0.24, 0.24, 0.24, 0.36, 0.48, 0.48, 0.8, 0.8, 0.53),
(33, 15010086, 0.32, 0.32, 0.32, 0.16, 0.36, 0.48, 0.48, 1, 1, 0.8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `spk_datakriteria`
--
ALTER TABLE `spk_datakriteria`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `spk_datanilai`
--
ALTER TABLE `spk_datanilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `spk_datarating`
--
ALTER TABLE `spk_datarating`
  ADD PRIMARY KEY (`id_rating`);

--
-- Indexes for table `spk_datasiswa`
--
ALTER TABLE `spk_datasiswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `spk_datauser`
--
ALTER TABLE `spk_datauser`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `spk_finalipa`
--
ALTER TABLE `spk_finalipa`
  ADD PRIMARY KEY (`id_nilai_final_ipa`);

--
-- Indexes for table `spk_finalips`
--
ALTER TABLE `spk_finalips`
  ADD PRIMARY KEY (`id_nilai_final_ips`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `spk_datakriteria`
--
ALTER TABLE `spk_datakriteria`
  MODIFY `id_kriteria` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `spk_datanilai`
--
ALTER TABLE `spk_datanilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT for table `spk_datarating`
--
ALTER TABLE `spk_datarating`
  MODIFY `id_rating` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `spk_datauser`
--
ALTER TABLE `spk_datauser`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `spk_finalipa`
--
ALTER TABLE `spk_finalipa`
  MODIFY `id_nilai_final_ipa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `spk_finalips`
--
ALTER TABLE `spk_finalips`
  MODIFY `id_nilai_final_ips` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
